# PROJECT INFO

**_TASK-MANAGER_**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

https://yadi.sk/i/bfb9cg6sh-cL3A

https://yadi.sk/i/hMmXNGj6Co_zsA

https://yadi.sk/i/5FN_w0Ukc_fVyA
